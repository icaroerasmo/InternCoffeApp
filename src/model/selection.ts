import { Intern } from '../model/intern'

export class Selection {
  id: number;
  intern: Intern;
  status: string;
  date: Date;

  constructor(id: number, intern: Intern, status: string, date: Date){
    this.id = id;
    this.intern = intern;
    this.status = status;
    this.date = date;
  }
}
