export class Intern {
  id: number;
  name: string;
  weight: number;
  checked: boolean;

  constructor(id: number, name: string, weight: number, checked: boolean){
    this.id = id;
    this.name = name;
    this.weight = weight;
    this.checked = checked;
  }
}
