import { Role } from '../model/role'

export class User{
  id: number;
  name: string;
  username: string;
  password: string;
  status: number;
  roles: Array<Role>;
  checked: boolean;

  constructor(id: number, name: string, username: string, password: string,
  status: number, roles: Array<Role>, checked: boolean) {
    this.id = id;
    this.name = name;
    this.username = username;
    this.password = password;
    this.status = status;
    this.roles = roles;
    this.checked = checked;
  }
}
