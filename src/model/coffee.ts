import { Intern } from '../model/intern'

export class Coffee {
  id: number;
  intern: Intern;
  date: Date;

  constructor(id: number, intern: Intern, date: Date){
    this.id = id;
    this.intern = intern;
    this.date = date;
  }
}
