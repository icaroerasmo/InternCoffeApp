import { Component } from '@angular/core';
import { DatePipe } from '@angular/common'
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CoffeeProvider } from '../../../providers/coffee/coffee';
import { UtilProvider } from '../../../providers/util/util';
import { Coffee } from '../../../model/coffee';

/**
* Generated class for the ShowCoffeePage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

@IonicPage()
@Component({
  selector: 'page-show-coffee',
  templateUrl: 'show-coffee.html',
})
export class ShowCoffeePage {

  coffees: Array<Coffee>

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public coffeeProvider: CoffeeProvider, public util: UtilProvider,
    public datepipe: DatePipe) {
      this.getCoffeeList();
    }

    ionViewDidLoad() {
    }

    getCoffeeList(){
      this.coffeeProvider.getCoffeeList().subscribe(
        (result: Array<Coffee>) => {
          this.coffees = result;
        },
        error => {
          this.util.showError('Não há cafés cadastrados.');
        },
        () => {
          for(let coffee of this.coffees){
            coffee['printableDate'] = this.datepipe.transform(coffee.date, 'dd/MM/yyyy \'às\' H:mm');
          }
        });
      }
    }
