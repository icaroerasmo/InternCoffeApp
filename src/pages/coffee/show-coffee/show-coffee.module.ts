import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowCoffeePage } from './show-coffee';

@NgModule({
  declarations: [
    ShowCoffeePage,
  ],
  imports: [
    IonicPageModule.forChild(ShowCoffeePage),
  ],
})
export class ShowCoffeePageModule {}
