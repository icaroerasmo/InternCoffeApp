import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowSelectionPage } from './show-selection';

@NgModule({
  declarations: [
    ShowSelectionPage,
  ],
  imports: [
    IonicPageModule.forChild(ShowSelectionPage),
  ],
})
export class ShowSelectionPageModule {}
