import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DatePipe } from '@angular/common'
import { UtilProvider } from '../../../providers/util/util';
import { SelectionProvider } from '../../../providers/selection/selection';
import { Selection } from '../../../model/selection';
/**
 * Generated class for the ShowSelectionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-show-selection',
  templateUrl: 'show-selection.html',
})
export class ShowSelectionPage {

  selections: Array<Selection>;

  constructor(public navCtrl: NavController, public navParams: NavParams,
  public selectionProvider: SelectionProvider, public datepipe: DatePipe,
  public util: UtilProvider) {
    this.loadSelections();
  }

  ionViewDidLoad() {
  }

  loadSelections(){
    this.selectionProvider.getRegisteredSelections().subscribe(
      (result: Array<Selection>) => {
        this.selections = result;
      },
      error => {
        this.util.showError('Não há seleções cadastradas.')
      },
      () => {
        for (let selection of this.selections) {
          if(selection.status === 'GAVE_UP'){
            selection.status = 'ARREGOU';
          } else if(selection.status == "COULDNT_MAKE"){
            selection.status = 'OCUPADO/AUSENTE';
          } else if(selection.status === "COFFEE_MADE"){
            selection.status = 'FEZ';
          }
          selection['printableDate'] = this.datepipe.transform(selection.date, 'dd/MM/yyyy \'às\' H:mm');
          }
        }
    )
  }

}
