import {Component, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams, Tabs } from 'ionic-angular';

/**
 * Generated class for the EditUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-user',
  templateUrl: 'edit-user.html'
})
export class EditUserPage {

    @ViewChild('myTabs') tabRef: Tabs;
    tab1Root = 'ChangeDetailsPage';
    tab2Root = 'ChangePasswordPage';
    tabParams: any;

    constructor(public navCtrl: NavController, public navParams: NavParams) {

      var user = this.navParams.get('user');
      this.tabParams = {user: user, navCtrl: this.navCtrl};

    }

  ionViewDidLoad() {

  }
}
