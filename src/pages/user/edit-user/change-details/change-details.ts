import { Component } from '@angular/core';
import { IonicPage, NavParams, NavController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { UserProvider } from '../../../../providers/user/user';
import { UtilProvider } from '../../../../providers/util/util';
import { RoleProvider } from '../../../../providers/role/role';
import { UsernameValidator } from '../../../../validators/username-validator';
import { Role } from '../../../../model/role';
import { User } from '../../../../model/user';

/**
 * Generated class for the ChangeDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-change-details',
  templateUrl: 'change-details.html',
})
export class ChangeDetailsPage {

  navCtrl: NavController;

  submitAttempt: any = false;
  editUserForm: any;
  validation_messages: any;
  user: User;

  roles: Array<Role>;

  nameRequiredMessage = { type: 'required', message: 'Nome é obrigatório.' };
  usernameRequiredMessage = { type: 'required', message: 'Usuário é obrigatório.' };
  usernameAndNameMinLengthMessage = { type: 'minlength', message: 'Mínimo de 3 caracteres.' };
  usernameAndNameMaxLengthMessage = { type: 'maxlength', message: 'Máximo de 8 caracteres.' };
  patternMessage = { type: 'pattern', message: 'Somente letras.' };
  usernameInUserMessage = { type: 'usernameInUse', message: 'Usuário já cadastrado.' };
  rolesRequiredMessage = { type: 'areEqual', message: 'Permissão é obrigatória.' }

  constructor(public navParams: NavParams, public userProvider: UserProvider,
  public util: UtilProvider, public formBuilder: FormBuilder,
  public usernameValidator: UsernameValidator, public roleProvider: RoleProvider) {

    this.user = this.navParams.get('user');
    this.navCtrl = this.navParams.get('navCtrl');

    this.editUserForm = formBuilder.group({

      username: [this.user.username, Validators.compose([Validators.minLength(3),
      Validators.maxLength(8), Validators.pattern('[a-zA-Z]*'),
      Validators.required]), usernameValidator.checkName.bind(usernameValidator)],

      name: [this.user.name, Validators.compose([Validators.minLength(3),
      Validators.maxLength(8), Validators.pattern(this.util.getFormRegex()),
      Validators.required])],

      roles: [this.user.roles[0], Validators.compose([Validators.required])]
    });

    this.validation_messages = {
      'username': [
        this.usernameRequiredMessage,
        this.patternMessage,
        this.usernameAndNameMinLengthMessage,
        this.usernameAndNameMaxLengthMessage,
        this.usernameInUserMessage
      ],
      'name': [
        this.nameRequiredMessage,
        this.patternMessage,
        this.usernameAndNameMinLengthMessage,
        this.usernameAndNameMaxLengthMessage
      ],
      'roles': [
        this.rolesRequiredMessage
      ]
    }

    this.loadRoles();
  }

  ionViewDidLoad() {
  }

  compareFn(r1: Role, r2: Role): boolean {
    return r1 && r2 ? r1.id === r2.id : r1 === r2;
  }

  loadRoles(){
    this.roleProvider.getRoleList().subscribe(
      (result: Array<Role>) =>{
        this.roles = result;
      },
      error => {
        this.util.showError('Erro ao recuperar lista de regras.');
      });
  }

  formIsInvalid () : boolean {
    return ((!this.editUserForm.controls.username.valid &&
      this.editUserForm.value.username !== this.user.username) ||
      !this.editUserForm.controls.name.valid ||
      !this.editUserForm.controls.roles.valid);
  }

  saveUser() {
    this.submitAttempt = true;

    if (!this.formIsInvalid()) {
      this.util.showLoading();

      var user = this.editUserForm.value;
      user.id = this.user.id;
      user.roles = [user.roles];
      user.status = 1;

      this.userProvider.editUser(user).subscribe(
        result => {
          this.close();
        },
        error => {
          this.util.showError('Erro ao inserir novo usuário');
        });
    }
  }

  close(){
    this.navCtrl.setRoot('ShowUserPage');
  }
}
