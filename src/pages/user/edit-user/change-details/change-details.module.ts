import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChangeDetailsPage } from './change-details';

@NgModule({
  declarations: [
    ChangeDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(ChangeDetailsPage),
  ],
})
export class ChangeDetailsPageModule {}
