import { Component } from '@angular/core';
import { IonicPage, NavParams, NavController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { UtilProvider } from '../../../../providers/util/util';
import { UserProvider } from '../../../../providers/user/user';
import { PasswordValidator } from '../../../../validators/password-validator';
import { User } from '../../../../model/user';
/**
 * Generated class for the ChangePasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html',
})
export class ChangePasswordPage {

  navCtrl: NavController;

  editPasswordForm: any;
  validation_messages: any;

  user: User;

  passwordMinLengthMessage = { type: 'minlength', message: 'Mínimo de 6 caracteres.' };
  passwordDoesntMatchMessage = { type: 'areEqual', message: 'Senhas não combinam.' };
  passwordRequiredMessage = { type: 'required', message: 'Senha é obrigatório.' };
  confirmPasswordRequiredMessage = { type: 'required', message: 'Confimação de senha é obrigatória.' };

  constructor(public navParams: NavParams,  public formBuilder: FormBuilder,
  public util: UtilProvider, public userProvider: UserProvider) {

    this.user = this.navParams.get('user');
    this.navCtrl = this.navParams.get('navCtrl');

    this.editPasswordForm = formBuilder.group({
      password: ['', Validators.compose([Validators.minLength(6),
         Validators.required])],

      confirmPassword: ['', Validators.compose([Validators.minLength(6),
        Validators.required])]
    }, {validator: PasswordValidator.areEqual});

    this.validation_messages = {
      'password': [
        this.passwordRequiredMessage,
        this.passwordMinLengthMessage,
        this.passwordDoesntMatchMessage
      ],
      'confirmPassword': [
        this.confirmPasswordRequiredMessage,
        this.passwordDoesntMatchMessage,
        this.passwordMinLengthMessage
      ],
      'formErrors':[
        this.passwordDoesntMatchMessage
      ]
    }
  }

  ionViewDidLoad() {
  }

  savePassword(){
    if (this.editPasswordForm.valid) {
      this.util.showLoading();

      var user = this.editPasswordForm.value;
      user.id = this.user.id;

      this.userProvider.editUser(user).subscribe(
        result => {
          this.close();
        },
        error => {
          this.util.showError('Erro ao inserir novo usuário');
        });
    }
  }

  close(){
    this.navCtrl.setRoot('ShowUserPage');
  }
}
