import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserProvider } from '../../../providers/user/user';
import { CoffeeProvider } from '../../../providers/coffee/coffee';
import { UtilProvider } from '../../../providers/util/util';
import { AuthServiceProvider } from '../../../providers/auth-service/auth-service';
import { User } from '../../../model/user';

/**
* Generated class for the ShowUserPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

@IonicPage()
@Component({
  selector: 'page-show-user',
  templateUrl: 'show-user.html',
})
export class ShowUserPage {

  users: Array<User>;
  itemsToDelete: Array<User>;
  hiddenItems: boolean;
  loggedUser: User;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public util: UtilProvider, public userProvider: UserProvider,
    public auth: AuthServiceProvider, public coffeeProvider: CoffeeProvider) {
      this.getUserList();
      this.itemsToDelete = new Array<User>();
      this.hiddenItems = true;
      this.loggedUser = this.auth.getUserInfo();
    }

    ionViewDidLoad() {
    }

    updateItems(user) {
      if (this.itemsToDelete.length > 0) {
        var index = this.itemsToDelete.indexOf(user);
        if (index > -1) {
          this.itemsToDelete.splice(index, 1);
        } else {
          this.itemsToDelete.push(user);
        }
      } else {
        this.itemsToDelete.push(user);
      }
    }

    selectItems() {
      if (this.hiddenItems) {
        for (let user of this.users) {
          user.checked = false;
        }
      }
      this.hiddenItems = !this.hiddenItems;
    }

    getUserList() {
      this.userProvider.getUsers().subscribe(
        (result: Array<User>) => {
          this.users = result;
        },
        error => {
          this.util.showError('Não há usuários cadastrados.')
        });
      }

      edit(user) {
        if (this.hiddenItems) {
          this.navCtrl.setRoot("EditUserPage", { user: user });
        }
      }

      showCreatePage() {
        this.navCtrl.setRoot("CreateUserPage")
      }

      select(user) {
        if(this.hiddenItems){
        this.util.presentActionSheet(user, [
          {
            text: 'Deletar',
            icon: 'md-trash',
            role: 'delete',
            handler: () => {
              this.userProvider.deleteUser(user).subscribe(
                result => {
                  this.util.showLoading();
                  this.getUserList();
                  this.util.dismiss();
                });
              }
            },
            {
              text: 'Cancel',
              icon: 'md-close',
              role: 'cancel',
            }
          ]);
          }
        }

        deleteMany() {
          this.util.showLoading();
          this.userProvider.deleteManyUsers(this.itemsToDelete).subscribe(
            result => {
              this.getUserList();
              this.hiddenItems = true;
              this.util.dismiss();
            });
          }
        }
