import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { UserProvider } from '../../../providers/user/user';
import { UtilProvider } from '../../../providers/util/util';
import { RoleProvider } from '../../../providers/role/role';
import { UsernameValidator } from '../../../validators/username-validator';
import { PasswordValidator } from '../../../validators/password-validator';
import { Role } from '../../../model/role';

/**
 * Generated class for the CreateUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-create-user',
  templateUrl: 'create-user.html',
})
export class CreateUserPage {

  submitAttempt: any = false;
  createUserForm: any;
  validation_messages: any;

  roles: Array<Role>;

  nameRequiredMessage = { type: 'required', message: 'Nome é obrigatório.' };
  usernameRequiredMessage = { type: 'required', message: 'Usuário é obrigatório.' };
  passwordRequiredMessage = { type: 'required', message: 'Senha é obrigatório.' };
  confirmPasswordRequiredMessage = { type: 'required', message: 'Confimação de senha é obrigatória.' };

  usernameAndNameMinLengthMessage = { type: 'minlength', message: 'Mínimo de 3 caracteres.' };
  usernameAndNameMaxLengthMessage = { type: 'maxlength', message: 'Máximo de 8 caracteres.' };

  passwordMinLengthMessage = { type: 'minlength', message: 'Mínimo de 6 caracteres.' };
  passwordDoesntMatchMessage = { type: 'areEqual', message: 'Senhas não combinam.' };

  patternMessage = { type: 'pattern', message: 'Somente letras.' };
  usernameInUserMessage = { type: 'usernameInUse', message: 'Usuário já cadastrado.' };

  rolesRequiredMessage = { type: 'required', message: 'Permissão é obrigatória.' }

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public userProvider: UserProvider, public util: UtilProvider,
    public formBuilder: FormBuilder, usernameValidator: UsernameValidator,
    public roleProvider: RoleProvider) {
    this.createUserForm = formBuilder.group({

      username: ['', Validators.compose([Validators.minLength(3),
      Validators.maxLength(8), Validators.pattern('[a-zA-Z]*'),
      Validators.required]), usernameValidator.checkName.bind(usernameValidator)],

      name: ['', Validators.compose([Validators.minLength(3),
      Validators.maxLength(8), Validators.pattern(this.util.getFormRegex()),
      Validators.required])],

      password: ['', Validators.compose([Validators.minLength(6),
         Validators.required])],

      confirmPassword: ['', Validators.compose([Validators.minLength(6),
        Validators.required])],

      roles: [{id: 2, role: 'USER'}, Validators.compose([Validators.required])]
    }, {validator: PasswordValidator.areEqual});

    this.validation_messages = {
      'username': [
        this.usernameRequiredMessage,
        this.patternMessage,
        this.usernameAndNameMinLengthMessage,
        this.usernameAndNameMaxLengthMessage,
        this.usernameInUserMessage
      ],
      'name': [
        this.nameRequiredMessage,
        this.patternMessage,
        this.usernameAndNameMinLengthMessage,
        this.usernameAndNameMaxLengthMessage
      ],
      'password': [
        this.passwordRequiredMessage,
        this.passwordMinLengthMessage,
        this.passwordDoesntMatchMessage
      ],
      'confirmPassword': [
        this.confirmPasswordRequiredMessage,
        this.passwordMinLengthMessage
      ],
      'roles': [
        this.rolesRequiredMessage
      ],
      'formErrors':[
        this.passwordDoesntMatchMessage
      ]
    }
    this.loadRoles();
  }

  ionViewDidLoad() {

  }

  loadRoles(){
    this.roleProvider.getRoleList().subscribe(
      (result: Array<Role>) =>{
        this.roles = result;
      },
      error => {
        this.util.showError('Erro ao recuperar lista de regras.');
      });
  }

  compareFn(r1: Role, r2: Role): boolean {
    return r1 && r2 ? r1.id === r2.id : r1 === r2;
  }

  save() {
    this.submitAttempt = true;

    if (this.createUserForm.valid) {
      this.util.showLoading();

      var user = this.createUserForm.value;
      user.roles = [user.roles];
      user.status = 1;
      user.id = null;

      this.userProvider.newUser(user).subscribe(
        result => {
          this.close();
        },
        error => {
          this.util.showError('Erro ao inserir novo usuário');
        });
    }
  }

  confirmPasswordError() : boolean{
    return this.createUserForm.value.password === this.createUserForm.value.confirmPassword;
  }

  close(){
    this.navCtrl.setRoot('ShowUserPage');
  }
}
