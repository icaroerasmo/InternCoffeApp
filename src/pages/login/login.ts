import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,
  AlertController, Events } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { UtilProvider } from '../../providers/util/util';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  registerCredentials = { username: '', password: '' };

  constructor(public navCtrl: NavController, public navParams: NavParams,
     public auth: AuthServiceProvider, public alertCtrl: AlertController,
     public events: Events, public util: UtilProvider) {
  }

  ionViewDidLoad() {
  }

  public login() {
    this.util.showLoading();
    this.auth.login(this.registerCredentials).subscribe(
      allowed => {
        if (allowed) {
          this.events.publish('user:logged');
          this.navCtrl.setRoot('HomePage');
        } else {
          this.util.showError("Access Denied");
        }
      },
      error => {
        this.util.showError(error);
      });
  }
}
