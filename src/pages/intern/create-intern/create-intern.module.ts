import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateInternPage } from './create-intern';

@NgModule({
  declarations: [
    CreateInternPage,
  ],
  imports: [
    IonicPageModule.forChild(CreateInternPage),
  ],
})
export class CreateInternPageModule {}
