import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { InternProvider } from '../../../providers/intern/intern';
import { UtilProvider } from '../../../providers/util/util';
import { NameValidator } from '../../../validators/name-validator';

/**
 * Generated class for the CreateInternPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-create-intern',
  templateUrl: 'create-intern.html',
})
export class CreateInternPage {

  submitAttempt: any = false;
  createInternForm: any;
  validation_messages: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public internProvider: InternProvider, public util: UtilProvider,
    public formBuilder: FormBuilder, nameValidator: NameValidator) {
    this.createInternForm = formBuilder.group({
      name: ['', Validators.compose([Validators.minLength(3),
      Validators.maxLength(8), Validators.pattern(this.util.getFormRegex()),
      Validators.required]), nameValidator.checkName.bind(nameValidator)],
    });

    this.validation_messages = {
      'name': [
        { type: 'required', message: 'Nome é obrigatório.' },
        { type: 'pattern', message: 'Somente letras.' },
        { type: 'minlength', message: 'Mínimo de 3 caracteres.' },
        { type: 'maxlength', message: 'Máximo de 8 caracteres.' },
        { type: 'nameInUse', message: 'Nome já cadastrado.' }
      ]
    }

  }

  ionViewDidLoad() {

  }

  save() {
    this.submitAttempt = true;

    if (this.createInternForm.valid) {
      this.util.showLoading();
      this.internProvider.newIntern(this.createInternForm.value).subscribe(
        result => {
          this.util.dismiss();
          this.close();
        },
        error => {
          this.util.dismiss();
          this.util.showError('Erro ao inserir novo usuário');
        });
    }
  }

  close(){
    this.navCtrl.setRoot('ShowInternPage');
  }
}
