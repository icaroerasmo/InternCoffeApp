import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { InternProvider } from '../../../providers/intern/intern';
import { CoffeeProvider } from '../../../providers/coffee/coffee';
import { UtilProvider } from '../../../providers/util/util';
import { Intern } from '../../../model/intern';

/**
* Generated class for the ShowInternPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

@IonicPage()
@Component({
  selector: 'page-show-intern',
  templateUrl: 'show-intern.html',
})
export class ShowInternPage {

  interns: Array<Intern>;
  itemsToDelete: Array<Intern>;
  hiddenItems: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public util: UtilProvider, public internProvider: InternProvider,
    public coffeeProvider: CoffeeProvider) {
    this.itemsToDelete = new Array<Intern>();
    this.hiddenItems = true;
    this.getInternList();
  }

  ionViewDidLoad() {
  }

  getInternList() {
    this.internProvider.getInterns().subscribe(
      (result: Array<Intern>) => {
        this.interns = result;
      },
      error => {
        this.util.showError('Não há estagiários cadastrados.')
      },
      () => {
        for (let intern of this.interns) {
          this.coffeeProvider.coffeeCounterByIntern(intern.id).subscribe(
            result => {
              intern['counterOfCoffee'] = result;
            });
        }
      });
  }

  selectItems() {
    if (this.hiddenItems) {
      for (let intern of this.interns) {
        intern.checked = false;
      }
    }
    this.hiddenItems = !this.hiddenItems;
  }

  select(intern) {
    if(this.hiddenItems){
    this.util.presentActionSheet(intern, [
      {
        text: 'Deletar',
        icon: 'md-trash',
        role: 'delete',
        handler: () => {
          this.internProvider.deleteIntern(intern).subscribe(
            result => {
              this.getInternList();
            });
          }
        },
        {
          text: 'Cancel',
          icon: 'md-close',
          role: 'cancel',
        }
      ]);
      }
    }

    deleteMany() {
      this.internProvider.deleteManyInterns(this.itemsToDelete).subscribe(
        result => {
          this.util.showLoading();
          this.getInternList();
          this.util.dismiss();
          this.hiddenItems = true;
        });
      }

  updateItems(intern) {
    if (this.itemsToDelete.length > 0) {
      var index = this.itemsToDelete.indexOf(intern);
      if (index > -1) {
        this.itemsToDelete.splice(index, 1);
      } else {
        this.itemsToDelete.push(intern);
      }
    } else {
      this.itemsToDelete.push(intern);
    }
  }

  edit(intern) {
    if (this.hiddenItems) {
      this.navCtrl.setRoot("EditInternPage", { intern: intern });
    }
  }

  showCreatePage() {
    this.navCtrl.setRoot("CreateInternPage");
  }
}
