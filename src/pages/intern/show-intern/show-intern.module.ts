import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowInternPage } from './show-intern';

@NgModule({
  declarations: [
    ShowInternPage,
  ],
  imports: [
    IonicPageModule.forChild(ShowInternPage),
  ],
})
export class ShowInternPageModule {}
