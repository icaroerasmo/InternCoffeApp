import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditInternPage } from './edit-intern';

@NgModule({
  declarations: [
    EditInternPage,
  ],
  imports: [
    IonicPageModule.forChild(EditInternPage),
  ],
})
export class EditInternPageModule {}
