import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { InternProvider } from '../../../providers/intern/intern';
import { UtilProvider } from '../../../providers/util/util';
import { NameValidator } from '../../../validators/name-validator';
import { Intern } from '../../../model/intern';

/**
 * Generated class for the EditInternPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-intern',
  templateUrl: 'edit-intern.html',
})
export class EditInternPage {

  submitAttempt: any = false;
  editInternForm: any;
  validation_messages: any;
  intern: Intern;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public internProvider: InternProvider, public util: UtilProvider,
    public formBuilder: FormBuilder, nameValidator: NameValidator) {

    this.intern = this.navParams.get('intern');

    if(this.intern){
      this.editInternForm = formBuilder.group({
        name: [this.intern.name, Validators.compose([Validators.minLength(3),
        Validators.maxLength(8), Validators.pattern(this.util.getFormRegex()),
        Validators.required]), nameValidator.checkName.bind(nameValidator)],
      });
    }else{
      this.navCtrl.setRoot("ShowInternPage");
    }

    this.validation_messages = {
      'name': [
        { type: 'required', message: 'Nome é obrigatório.' },
        { type: 'pattern', message: 'Somente letras.' },
        { type: 'minlength', message: 'Mínimo de 3 caracteres.' },
        { type: 'maxlength', message: 'Máximo de 8 caracteres.' },
        { type: 'nameInUse', message: 'Nome já cadastrado.' }
      ]
    }

  }

  ionViewDidLoad() {

  }

  save() {
    this.submitAttempt = true;
    if (this.editInternForm.valid) {
      this.util.showLoading();

      this.intern.name = this.editInternForm.value.name;

      this.internProvider.editIntern(this.intern).subscribe(
        result => {
          this.close();
        },
        error => {
          this.util.showError('Erro ao editar usuário');
        });
    }
  }

  close(){
    this.navCtrl.setRoot('ShowInternPage');
  }
}
