import { Component } from '@angular/core';
import { NavController, IonicPage, AlertController,
   LoadingController, Loading } from 'ionic-angular';
import { SelectionProvider } from '../../providers/selection/selection';
import { UtilProvider } from '../../providers/util/util';
import { Selection } from '../../model/selection';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  loading: Loading;
  selection : Selection;

  constructor(public navCtrl: NavController,
    public selectionProvider: SelectionProvider,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public util: UtilProvider) {
  }

  pickUpIntern() {
    this.util.showLoading();
    this.selectionProvider.getNextIntern().subscribe(
      (next : Selection) => {
        this.util.dismiss();
        this.selection = next;
        this.showInternPicking();
      });
  }

  showInternPicking() {
    let alert = this.alertCtrl.create({
      title: this.selection.intern.name,
      subTitle: 'sorteado',
      buttons: [{
        text: 'Fez café',
        handler: () => {
          this.selection.status = 'COFFEE_MADE';
          this.selectionProvider.
          sendSelection(this.selection).subscribe();

          this.selectionProvider.reset().subscribe();
        }
      },
      {
        text: 'Arregou',
        handler: () => {
          this.selection.status = 'GAVE_UP';
          this.selectionProvider.
          sendSelection(this.selection).subscribe(
            result => {
              this.pickUpIntern();
            }
          );
        }
      },
      {
        text: 'Não pôde fazer',
        handler: () => {
          this.selection.status = 'COULDNT_MAKE';
          this.selectionProvider.
          sendSelection(this.selection).subscribe(
            result => {
              this.pickUpIntern();
            }
          );
        }
      },
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          this.selectionProvider.reset().subscribe();
        }
      },]
    });
    alert.present(prompt);
  }
}
