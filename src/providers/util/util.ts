import { LoadingController, Loading, AlertController, ActionSheetController } from 'ionic-angular'
import { Injectable } from '@angular/core';

@Injectable()
export class UtilProvider{

  private loading: Loading;
  private apiUrl : string = /*'http://localhost:8080/'*/'https://api-interncoffeeprojekt.7e14.starter-us-west-2.openshiftapps.com/';

  constructor(public loadingCtrl: LoadingController, public actionSheetCtrl: ActionSheetController,
  public alertCtrl: AlertController){
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  dismiss(){
    this.loading.dismiss();
  }

  showError(text) {
    if(typeof this.loading != 'undefined'){
      this.loading.dismiss();
    }
    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  presentActionSheet(entity, buttons) {

    var employee;

    if(entity.username){
      employee = 'Usuário ';
    }else {
      employee = 'Estagiário '
    }

    let actionSheet = this.actionSheetCtrl.create({
      title: employee + entity.name,
      buttons: buttons
    });
    actionSheet.present();
  }

  getFormRegex() : string{
    return '[a-zA-Z\u00C0-\u00FF ]+';
  }

  getApiUrl() : string{
    return this.apiUrl;
  }

  getLoginUrl() : string{
    return this.apiUrl + 'login';
  }

  getLogoutUrl() : string{
    return this.apiUrl + 'logout';
  }

  getSelectionUrl() : string{
    return this.apiUrl + 'selection/';
  }

  getInternUrl() : string{
    return this.apiUrl + 'intern/';
  }

  getUserUrl() : string{
    return this.apiUrl + 'user/';
  }

  getRoleUrl() : string{
    return this.apiUrl + 'role/';
  }

  getCoffeeUrl() : string{
    return this.apiUrl + 'coffee/';
  }

  getInternByNameUrl() : string{
    return this.getInternUrl() + 'byName';
  }

  getUserByUsernameUrl() : string{
    return this.getUserUrl() + 'byUsername';
  }

  getCounterOfCoffeeByIntern() : string{
    return this.getCoffeeUrl() + 'counterByIntern';
  }

  getNextSelectionUrl() : string{
    return this.getSelectionUrl()+'next';
  }

  getRegisteredSelectionsUrl() : string{
    return this.getSelectionUrl()+'registered';
  }

  getResetUrl() : string{
    return this.getSelectionUrl()+'reset';
  }
}
