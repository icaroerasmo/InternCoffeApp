import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UtilProvider } from '../util/util'

/*
Generated class for the InternProvider provider.

See https://angular.io/guide/dependency-injection for more info on providers
and Angular DI.
*/
@Injectable()
export class SelectionProvider {

  constructor(public http : HttpClient, public util : UtilProvider) {
  }

  getNextIntern() {
    return this.http.get(this.util.getNextSelectionUrl());
  }

  reset() {
    return this.http.get(this.util.getResetUrl(),
    { responseType: 'text' });
  }

  sendSelection(selection) {
    return this.http.post(this.util.getSelectionUrl(), selection);
  }

  getRegisteredSelections() {
    return this.http.get(this.util.getRegisteredSelectionsUrl());
  }
}
