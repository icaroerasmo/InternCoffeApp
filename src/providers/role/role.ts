import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UtilProvider } from '../util/util';

/*
  Generated class for the RoleProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RoleProvider {

  constructor(public http: HttpClient, public util: UtilProvider) {
  }

  getRoleList(){
    return this.http.get(this.util.getRoleUrl());
  }

}
