import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { UtilProvider } from '../util/util'
import { User } from '../../model/user';
/*
Generated class for the AuthServiceProvider provider.

See https://angular.io/guide/dependency-injection for more info on providers
and Angular DI.
*/

@Injectable()
export class AuthServiceProvider {

  currentUser: User;

  constructor(public http: HttpClient, public util: UtilProvider) {
  }

  public login(credentials) {
    if (credentials.username === null || credentials.password === null) {
      return Observable.throw("Please insert credentials");
    } else {
      return Observable.create(observer => {

        const body = new HttpParams()
        .set('username', credentials.username)
        .set('password', credentials.password);

        this.http.post(this.util.getLoginUrl(),
        body.toString(), {headers: new HttpHeaders()
          .set('Content-Type', 'application/x-www-form-urlencoded')
        }).subscribe(
          (result:any) => {
            this.currentUser = result;
            observer.next(true);
            observer.complete()},
            error => {
              this.currentUser = null;
              observer.next(false);
              observer.complete()
            });
          });
        }
      }

      public getUserInfo() : User {
        return this.currentUser;
      }

      public logout() {
        return Observable.create(
          observer => {
            this.currentUser = null;
            this.http.get(this.util.getLogoutUrl(),
            { responseType: 'text' }).subscribe(
              result => {
                observer.next(true);
                observer.complete();
              });
            });
          }
        }
