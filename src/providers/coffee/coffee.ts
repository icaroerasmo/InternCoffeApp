import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UtilProvider } from '../util/util';

/*
  Generated class for the CoffeeProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CoffeeProvider {

  constructor(public http: HttpClient, public util: UtilProvider) {
  }

  getCoffeeList(){
    return this.http.get(this.util.getCoffeeUrl());
  }

  coffeeCounterByIntern(id){
    return this.http.get(this.util.getCounterOfCoffeeByIntern(), {
        params: new HttpParams().set('internId', id),
        responseType: 'text'
    });
  }
}
