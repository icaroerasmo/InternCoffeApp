import { HttpClient, HttpParams, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UtilProvider } from '../util/util';

/*
  Generated class for the InternProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class InternProvider {

  constructor(public http: HttpClient, public util : UtilProvider) {
  }

  newIntern(intern){
    return this.http.post(this.util.getInternUrl(), intern);
  }

  editIntern(intern){
    return this.http.patch(this.util.getInternUrl(), intern);
  }

  getByName(name){
    return this.http.get(this.util.getInternByNameUrl(), {
        params: new HttpParams().set('name', name)
    });
  }

  getInterns(){
    return this.http.get(this.util.getInternUrl());
  }

  deleteIntern(intern){
    return this.http.delete(this.util.getInternUrl()+intern.id,
    { responseType: 'text' });
  }

  deleteManyInterns(interns){
    return this.http.request(new HttpRequest('DELETE', this.util.getInternUrl(), interns, {
      responseType:'text'
    }));
  }
}
