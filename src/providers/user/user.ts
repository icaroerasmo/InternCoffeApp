import { HttpClient, HttpParams, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UtilProvider } from '../util/util';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {

  constructor(public http: HttpClient, public util : UtilProvider) {
  }

  newUser(user){
    return this.http.post(this.util.getUserUrl(), user);
  }

  editUser(user){
    return this.http.patch(this.util.getUserUrl(), user);
  }

  getByUsername(username){
    return this.http.get(this.util.getUserByUsernameUrl(), {
        params: new HttpParams().set('username', username)
    });
  }

  getUsers(){
    return this.http.get(this.util.getUserUrl());
  }

  deleteUser(user){
    return this.http.delete(this.util.getUserUrl()+user.id,
    { responseType: 'text' });
  }

  deleteManyUsers(users){
    return this.http.request(new HttpRequest('DELETE', this.util.getUserUrl(), users, {
      responseType:'text'
    }));
  }
}
