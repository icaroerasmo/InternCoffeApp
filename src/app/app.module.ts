import { HttpClientModule } from '@angular/common/http';
import { DatePipe } from '@angular/common'
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { LoginPageModule } from '../pages/login/login.module';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { SelectionProvider } from '../providers/selection/selection';
import { UtilProvider } from '../providers/util/util';
import { InternProvider } from '../providers/intern/intern';
import { NameValidator } from '../validators/name-validator';
import { UsernameValidator } from '../validators/username-validator';
import { CoffeeProvider } from '../providers/coffee/coffee';
import { UserProvider } from '../providers/user/user';
import { RoleProvider } from '../providers/role/role';

@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    LoginPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthServiceProvider,
    DatePipe,
    UserProvider,
    InternProvider,
    CoffeeProvider,
    SelectionProvider,
    UtilProvider,
    NameValidator,
    UsernameValidator,
    RoleProvider
  ]
})
export class AppModule {}
