import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { UtilProvider } from '../providers/util/util'
import { AuthServiceProvider } from '../providers/auth-service/auth-service'
import { LoginPage } from '../pages/login/login';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage:any = LoginPage;
  pages: Array<{title: string, component: any}>;
  name = '';
  username = '';

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public events: Events,
    public auth: AuthServiceProvider,
    public util: UtilProvider
     ) {
      platform.ready().then(
        () => {
          // Okay, so the platform is ready and our plugins are available.
          // Here you can do any higher level native things you might need.
          statusBar.styleDefault();
          splashScreen.hide();
        });
        this.pages = [
          { title: 'Home', component: 'HomePage' },
          { title: 'Usuários', component: 'ShowUserPage' },
          { title: 'Estagiários', component: 'ShowInternPage' },
          { title: 'Registro de Cafés', component: 'ShowCoffeePage' },
          { title: 'Registro de seleções de café', component: 'ShowSelectionPage'}
        ];
        events.subscribe('user:logged', () => {
          this.setUserData();
        });
        // used for an example of ngFor and navigation
      }

      openPage(page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
      }

      setUserData(){
        var user = this.auth.getUserInfo();
        if(typeof user != 'undefined'){
          this.name = user.name;
          this.username = user.username;
        }
      }

      public logout() {
        this.util.showLoading();
        this.auth.logout().subscribe(
        succ => {
          this.nav.setRoot('LoginPage')
          this.util.dismiss();
        });
      }
    }
