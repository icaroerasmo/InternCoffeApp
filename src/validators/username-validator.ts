import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';
import { UserProvider } from '../providers/user/user';

@Injectable()
export class UsernameValidator {

  debouncer: any;

  constructor(public userProvider: UserProvider) {

  }

  checkName(control: FormControl, username: String): any {

    return new Promise(
      resolve => {
      clearTimeout(this.debouncer);
      this.debouncer = setTimeout(() => {
        this.userProvider.getByUsername(control.value)
        .subscribe(
          (res) => {
            if(control.value != username){
              resolve({ 'usernameInUse': true });
            }else{
              resolve({ 'usernameInUse': true });
            }
        }, (err) => {
          resolve(null);
        });
      }, 1000);
    });
  }

}
