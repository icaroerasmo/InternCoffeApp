import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';
import { InternProvider } from '../providers/intern/intern';

@Injectable()
export class NameValidator {

  debouncer: any;

  constructor(public internProvider: InternProvider) {

  }

  checkName(control: FormControl): any {

    return new Promise(
      resolve => {
      clearTimeout(this.debouncer);
      this.debouncer = setTimeout(() => {
        this.internProvider.getByName(control.value)
        .subscribe(
          (res) => {
          resolve({ 'nameInUse': true });
        }, (err) => {
          resolve(null);
        });
      }, 1000);
    });
  }

}
